const crearMensaje = (nombre, mensaje) => {

    var fecha = new Date();
    return {
        nombre,
        mensaje,
        fecha: ("0" + fecha.getDate()).slice(-2)+"/"+("0" + (fecha.getMonth() + 1)).slice(-2)+"/"+fecha.getFullYear(),
        hora: fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds()
    }
}

module.exports = {
    crearMensaje
}